package com.tietkiemnhom.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import web.utilities.Common;
import java.awt.*;
import java.awt.event.InputEvent;
import java.util.*;

/**
 * Created by TamTran on 6/2/2017.
 */
public class TietKiemNhom {
    private WebDriver driver;
    private Robot robot;
    //private java.util.List<WebElement> elementList;

    @FindBy(name = "phone")
    WebElement phoneInput;

    @FindBy(name = "password")
    WebElement passwordInput;

    @FindBy(xpath = "//div[@id='route']//button[contains(@class,'mdl-button--colored')]")
    WebElement tieptucButton;

    @FindBy(xpath = "//div[@id='route']//i[contains(@class,'material-icons')]")
    WebElement clicksubMenu;

    @FindBy(xpath = "//a[@href='/logout']//div[contains(@class,'DHuSU-')]")
    WebElement thoatButton;

    public TietKiemNhom(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public TietKiemNhom handlePopUp() {
        try {
            robot = new Robot();
            //robot.keyPress(KeyEvent.VK_ESCAPE);
            robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        } catch (AWTException e) {
            e.printStackTrace();
        }
        return this;
    }

    public TietKiemNhom resetData() {
        //driver.findElement(By.name("phone")).clear();
        //driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("phone")).sendKeys(Keys.CONTROL+"a");
        driver.findElement(By.name("phone")).sendKeys(Keys.DELETE);

        driver.findElement(By.name("password")).sendKeys(Keys.CONTROL+"a");
        driver.findElement(By.name("password")).sendKeys(Keys.DELETE);

        return this;
    }

    public TietKiemNhom typePhone(String text) {
        phoneInput.sendKeys(text);
        return this;
    }

    public TietKiemNhom typePassword(String text) {
        passwordInput.sendKeys(text);
        return this;
    }

    public TietKiemNhom clickTiepTuc() {
        tieptucButton.click();
        return this;
    }

    public TietKiemNhom clickThoat() {

        clicksubMenu.click();
        Common.sleep(2000);
        //WebDriverWait wait = new WebDriverWait(driver, 10);
        //wait.until(ExpectedConditions.elementToBeSelected(By.xpath("//div[@id='route']//i[contains(@class,'material-icons')]")));
        thoatButton.click();
        return this;
    }



}
