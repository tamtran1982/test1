package web.utilities;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by TamTran on 12/24/2016.
 */
public class ExcelUtils {
    public static String filePath;
    public static Workbook workbook;
    public static Sheet sheet;
    public static Row row;
    public static Cell cell;

    private static void setWorkbookExcel(String inFilePath) {
        filePath = inFilePath;
        try {
            FileInputStream inputStream = new FileInputStream(filePath);
            switch (FilenameUtils.getExtension(filePath).toLowerCase()) {
                case "xlsx":
                    workbook = new XSSFWorkbook(inputStream);
                    break;
                case "xls":
                    workbook = new HSSFWorkbook(inputStream);
                    break;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set an Excel workbook from based on file path and sheet name.
     */
    public static void setWorkbook(String filePath, String sheetName) {
        setWorkbookExcel(filePath);
        sheet = workbook.getSheet(sheetName);
    }

    /**
     * Set an Excel workbook from based on file path and sheet index.
     */
    public static void setWorkbook(String filePath, int sheetIndex) {
        setWorkbookExcel(filePath);
        sheet = workbook.getSheetAt(sheetIndex);
    }

    /**
     * Read a cell data from the Excel file based on the row index and the column index.
     */
    public static String getCellValue(int rowIndex, int columnIndex) {
        try {
            cell = sheet.getRow(rowIndex).getCell(columnIndex);
            return cell.getStringCellValue();
        } catch (Exception e) {
            return e.toString();
        }
    }

    /**
     * Write a cell data to the Excel file.
     */
    public static void setCellValue(String value, int rowIndex, int columnIndex) {
        row = sheet.getRow(rowIndex);
        cell = row.getCell(columnIndex, row.RETURN_BLANK_AS_NULL);

        if (cell == null)
            cell = row.createCell(columnIndex);
        cell.setCellValue(value);
    }

    /**
     * Save As a new Excel file
     */
    public static void saveAs(String filePath) {
        FileOutputStream fileOut = null;
        try {
            fileOut = new FileOutputStream(filePath);
            workbook.write(fileOut);
            fileOut.flush();
            fileOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Return the column number for a given work sheet where the header text contains headerColumnText
     */
    public static int getColumnIndex(XSSFSheet sheet, String headerColumnText) {
        final int index = 0;
        final XSSFRow headerRow = sheet.getRow(0);
        final Iterator<Cell> cellIterator = headerRow.cellIterator();
        while (cellIterator.hasNext()) {
            final XSSFCell cell = (XSSFCell) cellIterator.next();
            if (cell.getStringCellValue().equalsIgnoreCase(headerColumnText.toLowerCase())) {
                return cell.getColumnIndex();
            }
        }
        return index;
    }

    /**
     * Get a list of all values from the specified column
     */
    public static ArrayList<String> getColumnValues(XSSFSheet s, int index) {
        final ArrayList<String> cellValues = new ArrayList<String>();
        final int rowCount = s.getLastRowNum();
        for (int i = 0; i <= rowCount - 1; i++) {
            try {
                String cellValue = s.getRow(i).getCell(index).getStringCellValue();
                cellValues.add(cellValue);
            } catch (Exception e) {
                cellValues.add("");
            }
        }
        return cellValues;
    }

//    public Cell getCellMatchTextCondition(String text) {
//        sheet = workbook.getSheet(0);
//
//    }

}
