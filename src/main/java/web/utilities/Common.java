package web.utilities;

/**
 * Created by TamTran on 12/24/2016.
 */
public class Common {
    /**
     * Sleep for certain milliseconds
     */
    public static void sleep(int i) {
        try {
            Thread.sleep(i);
        } catch (final InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
}
